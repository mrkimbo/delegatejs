# DelegateJS


Lightweight library for creating and managing scope-bound delegate functions.
This library attempts to solve the common problem of both quickly creating anonymous functions for binding callback scope
when adding listeners to objects and also removing the listener at some later point.

References to scope objects are also cleaned out when no longer required.

> Although this library was written before discovering Function.bind, it still solves the problem of removing / cleaning up anonymous callbacks

Example usage:
```javascript
// add a click event handler bound to current scope //
myObject.addEventListener(
  'click', Delegate.create('click', this.clickHandler, this)
);

// remove click handler //
myObject.removeEventListener(
  'click', Delegate.remove('click', this.clickHandler, this);
);
```