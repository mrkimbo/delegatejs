/**
 * Created with JetBrains WebStorm.
 * User: kim holland - blogstuff@tripleaxis.com
 * Date: 13/09/13
 * Time: 5:53 PM
 *
 * Delegate system for binding functions to scope with the ability to
 * retrieve them when removing listeners.
 *
 * Credit for string hashing function to:
 * https://github.com/darkskyapp/string-hash
 */

'use strict';

var Delegate = {

  /** {Object} Internal delegate storage object. @private */
  _: {},

  /**
   * Check if object contains values
   * @param {Object} obj
   * @returns {boolean}
   */
  _isEmpty: function(obj)
  {
    var i;
    for(i in obj) return false
    return true;
  },

  /**
   * Create hash from event name and function in order to optimise storage.
   * @param {string} n Event Name
   * @param {Function|string} f The callback function
   * @returns {number} Hash
   * @private
   */
  _hash: function(e,f)
  {
    var h = 5381, s = e+f.toString(), i = s.length;
    while(i) h = (h*33)^s.charCodeAt(--i);
    return h >>> 0;
  },

  /**
   * Add an item to the store.
   * @param {Object|Function} scope Callback execution scope
   * @param {String} evt Event Name
   * @param {Function} fn Raw callback function
   * @param {Function} f Generated delegate function
   * @returns {Function} Generated delegate function
   * @private
   */
  _set: function(scope, evt, fn, f)
  {
    if(!this._[scope]) this._[scope] = {};
    this._[scope][this._hash(evt,fn)] = f;
    return f;
  },

  /**
   * Create and store a delegate function.
   * @param {string} evt Event Name
   * @param {Function} fn Callback function
   * @param {Object|Function} scope Callback execution scope
   * @param {..args} args Arguments to be passed to callback
   * @returns {Function} Generated delegate function
   */
  create: function(evt, fn, scope, args)
  {
    var a = Array.prototype.slice.call(arguments, 3);
    var f = a.length ?
      function (){
        var d = Array.prototype.slice.call(arguments);
        return Array.prototype.unshift.apply(d, a), fn.apply(scope, d)
      } :
      function (){
        return fn.apply(scope, arguments)
      };

    return this._set(scope, evt, fn, f);
  },

  /**
   * Remove and return a previously created delegate.
   * Clean up any unused scopes to prevent unnecessary reference caching.
   * @param {string} evt Event Name
   * @param {Function} fn Callback function
   * @param {Object|Function} scope Callback execution scope
   * @returns {*}
   */
  remove: function(evt, fn, scope)
  {
    var h = this._hash(evt,fn);
    try{ var f = this._[scope][h]; }catch(e){ return null; }
    delete this._[scope][h];
    if(this._isEmpty(this._[scope][h])) delete this._[scope];
    return f;
  }
};
